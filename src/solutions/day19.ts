import { Day, run_tests } from "./base";

interface Replacements {
    [input: string]: string[];
}

interface Node {
    molecule: string;
    steps: number;
}

export class Day19 extends Day {
    nday = 19;

    parse_input(input: string[]): Replacements {
        const replacements: Replacements = {};
        for (const line of input) {
            const [i, r] = line.split(" => ");
            if (typeof replacements[i] === "undefined") {
                replacements[i] = [];
            }
            replacements[i].push(r);
        }
        return replacements;
    }

    get_distinct_molecules(molecule: string, replacements: Replacements): Set<string> {
        let results: Set<string> = new Set();
        for (const from in replacements) {
            let ix: number[] = [];
            let regex = RegExp(from, "g");
            let match: RegExpMatchArray;
            while ((match = regex.exec(molecule))) {
                ix.push(match.index);
            }

            for (const i of ix) {
                if (molecule.substr(i, from.length) === from) {
                    for (const to of replacements[from]) {
                        const new_molecule = molecule.substr(0, i) + to + molecule.substr(i + from.length);
                        if (!results.has(new_molecule)) {
                            results.add(new_molecule);
                        }
                    }
                }
            }
        }
        return results;
    }

    find_min_steps_medicine(molecule: string, expected: string, replacements: Replacements): number {
        let queue: Node[] = [];
        let checked: Set<string> = new Set();

        const new_molecules = this.get_distinct_molecules(molecule, replacements);
        for (const m of new_molecules) {
            checked.add(m);
            queue.push({ molecule: m, steps: 1 });
        }

        while (queue.length > 0) {
            const node = queue.shift();
            if (node.molecule.length > expected.length) {
                continue;
            }
            if (node.molecule === expected) {
                return node.steps;
            }

            let molecules = [...this.get_distinct_molecules(node.molecule, replacements)];
            molecules.sort((x, y) => {
                if (x.length > y.length) {
                    return -1;
                }
                if (x.length < y.length) {
                    return 1;
                }
                return 0;
            });

            let c = 0;
            for (const m of molecules) {
                if (!checked.has(m)) {
                    queue.push({ molecule: m, steps: node.steps + 1 });
                    checked.add(m);
                    c++;
                    if (c === 6) {
                        break;
                    }
                }
            }
        }
    }

    tests(): boolean {
        let test_replacements = ["H => HO", "H => OH", "O => HH"];
        let replacements = this.parse_input(test_replacements);

        let test_inputs = ["HOH", "HOHOHO"];
        let test_results = [4, 7];

        if (!run_tests(test_inputs, test_results, (input) => this.get_distinct_molecules(input, replacements).size)) {
            return false;
        }

        test_replacements = ["e => H", "e => O", "H => HO", "H => OH", "O => HH"];
        replacements = this.parse_input(test_replacements);

        if (this.find_min_steps_medicine("e", "HOHOHO", replacements) !== 6) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        const [input_replacements, input_molecule] = input.split("\n\n");
        const replacements = this.parse_input(input_replacements.split("\n"));
        return this.get_distinct_molecules(input_molecule, replacements).size;
    }

    part2(input: string): number {
        // https://www.reddit.com/r/adventofcode/comments/3xflz8/day_19_solutions/cy4etju/
        const [_, input_molecule] = input.split("\n\n");

        let n = 0;
        let m = 0; // Rn/Ar
        let l = 0; // Y
        for (let i = 0; i < input_molecule.length; i++) {
            const c = input_molecule[i];
            if (c.toUpperCase() === c) {
                n++;
            }

            if (c === "Y") {
                console.log(c);
                l++;
            }

            if (i < input_molecule.length - 1) {
                const next = input_molecule[i + 1];
                if (c + next === "Ar" || c + next === "Rn") {
                    m++;
                }
            }
        }

        console.log(n, m, l);
        return n - m - 2 * l - 1;
    }
}
