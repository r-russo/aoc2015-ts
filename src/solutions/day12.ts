import { Day, run_tests } from "./base";

export class Day12 extends Day {
    nday = 12;

    find_sum_of_nums(input: string): number {
        let current_num = "";
        let sum = 0;

        for (const c of input) {
            if (!isNaN(parseInt(c))) {
                current_num += c;
            } else if (current_num === "" && c === "-") {
                current_num = "-";
            } else {
                const n = parseInt(current_num);
                if (!isNaN(n)) {
                    sum += n;
                }
                current_num = "";
            }
        }

        if (current_num !== "") {
            sum += parseInt(current_num);
        }

        return sum;
    }

    find_sum_of_nums2(input: string): number {
        // base case
        if (!input.match(/[\[\{\}\]]+/)) {
            if (!input.match(/(red)+/)) {
                return this.find_sum_of_nums(input);
            } else {
                return 0;
            }
        }

        let sum = 0;
        let outer_buffer = "";
        let inner_buffers: string[] = [""];

        let level = 0;
        for (const c of input) {
            if (c === "{" || c === "[") {
                level++;
            }

            if (level === 1) {
                outer_buffer += c;
            } else {
                inner_buffers[inner_buffers.length - 1] += c;
            }

            if (c === "}" || c == "]") {
                level--;
                if (level === 1) {
                    inner_buffers.push("");
                }
            }
        }

        if (outer_buffer[0] != "[" && outer_buffer.match(/(red)+/)) {
            return 0;
        }

        sum += this.find_sum_of_nums(outer_buffer);

        for (const buffer of inner_buffers) {
            sum += this.find_sum_of_nums2(buffer);
        }

        return sum;
    }

    tests(): boolean {
        let test_inputs = [
            "[1,2,3]",
            '{"a":2,"b":4}',
            "[[[3]]]",
            '{"a":{"b":4},"c":-1}',
            '{"a":[-1,1]}',
            '[-1,{"a":1}]',
            "[]",
            "{}",
        ];
        let test_results = [6, 6, 3, 3, 0, 0, 0, 0];

        if (!run_tests(test_inputs, test_results, this.find_sum_of_nums)) {
            return false;
        }

        test_inputs = [
            "[1,2,3]",
            '[1,{"c":"red","b":2},3]',
            '{"d":"red","e":[1,2,3,4],"f":5}',
            '[1,"red",5]',
            "{3, {red, {4}, 2}}",
            "{6,[red, 3], {red, 2}, 2, {1, {red, {4}, 2}}",
        ];
        test_results = [6, 4, 0, 6, 3, 12];

        if (!run_tests(test_inputs, test_results, (input: string) => this.find_sum_of_nums2(input))) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        return this.find_sum_of_nums(input);
    }

    part2(input: string): number {
        return this.find_sum_of_nums2(input);
    }
}
