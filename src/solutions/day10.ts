import { Day, run_tests } from "./base";

export class Day10 extends Day {
    nday = 10;

    look_and_say(n: string): string {
        let last_digit = n[0];
        let count = 1;
        let result = "";
        for (const digit of n.substr(1, n.length)) {
            if (last_digit === digit) {
                count++;
            } else {
                result += `${count}${last_digit}`;
                last_digit = digit;
                count = 1;
            }
        }

        result += `${count}${last_digit}`;

        return result;
    }

    tests(): boolean {
        let test_inputs = ["1", "11", "21", "1211", "111221"];
        let test_results = ["11", "21", "1211", "111221", "312211"];

        if (!run_tests(test_inputs, test_results, this.look_and_say)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        for (let i = 0; i < 40; i++) {
            input = this.look_and_say(input);
        }
        return input.length;
    }

    part2(input: string): number {
        for (let i = 0; i < 50; i++) {
            input = this.look_and_say(input);
        }
        return input.length;
    }
}
