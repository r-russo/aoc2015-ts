import { Day } from "./base";

interface Route {
    [from: string]: { [to: string]: number };
}

export class Day9 extends Day {
    nday = 9;

    parse_routes(routes: string[]): Route {
        let r: Route = {};
        for (const route of routes) {
            const [from, _1, to, _2, distance] = route.split(" ");
            const int_distance = parseInt(distance);
            if (typeof r[from] === "undefined") {
                r[from] = {};
            }

            if (typeof r[to] === "undefined") {
                r[to] = {};
            }
            r[from][to] = int_distance;
            r[to][from] = int_distance;
        }
        return r;
    }

    get_permutations(k: number, locations: string[], permutations: string[][]) {
        if (k === 1) {
            permutations.push(locations.slice());
        } else {
            this.get_permutations(k - 1, locations, permutations);

            for (let i = 0; i < k - 1; i++) {
                if (k % 2 === 0) {
                    const tmp = locations[i];
                    locations[i] = locations[k - 1];
                    locations[k - 1] = tmp;
                } else {
                    const tmp = locations[0];
                    locations[0] = locations[k - 1];
                    locations[k - 1] = tmp;
                }
                this.get_permutations(k - 1, locations, permutations);
            }
        }
    }

    path_distances(graph: Route): number[] {
        let locations: string[] = [];
        let distances: number[] = [];

        for (const location in graph) {
            locations.push(location);
        }
        let permutations: string[][] = [];
        this.get_permutations(locations.length, locations, permutations);

        for (const permutation of permutations) {
            let distance = 0;
            for (let i = 0; i < permutation.length - 1; i++) {
                distance += graph[permutation[i]][permutation[i + 1]];
            }
            distances.push(distance);
        }

        return distances;
    }

    tests(): boolean {
        let test_input = ["London to Dublin = 464", "London to Belfast = 518", "Dublin to Belfast = 141"];
        let graph = this.parse_routes(test_input);
        const distances = this.path_distances(graph);
        if (Math.min(...distances) !== 605) {
            return false;
        }

        if (Math.max(...distances) !== 982) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        let graph = this.parse_routes(input.split("\n"));
        const distances = this.path_distances(graph);
        return Math.min(...distances);
    }

    part2(input: string): number {
        let graph = this.parse_routes(input.split("\n"));
        const distances = this.path_distances(graph);
        return Math.max(...distances);
    }
}
