import { Day } from "./base";

interface Stats {
    hp: number;
    damage: number;
    armor: number;
}

interface Equipment {
    name: string;
    cost: number;
    damage: number;
    armor: number;
}

const weapon_table = ["Dagger 8 4 0", "Shortsword 10 5 0", "Warhammer 25 6 0", "Longsword 40 7 0", "Greataxe 74 8 0"];

const armor_table = [
    "None 0 0 0",
    "Leather 13 0 1",
    "Chainmail 31 0 2",
    "Splintmail 53 0 3",
    "Bandedmail 75 0 4",
    "Platemail 102 0 5",
];

const rings_table = [
    "None 0 0 0",
    "Defense+1 20 0 1",
    "Damage+1 25 1 0",
    "Defense+2 40 0 2",
    "Damage+2 50 2 0",
    "Defense+3 80 0 3",
    "Damage+3 100 3 0",
];

const parse_table = (table: string[]): Equipment[] => {
    let eq: Equipment[] = [];
    for (const line of table) {
        const [name, cost, damage, armor] = line.split(" ");
        eq.push({
            name: name.trim(),
            cost: parseInt(cost.trim()),
            damage: parseInt(damage.trim()),
            armor: parseInt(armor.trim()),
        });
    }
    return eq;
};

export class Day21 extends Day {
    nday = 21;

    parse_enemy_stats(input: string[]): Stats {
        let stats: Stats = { hp: 0, damage: 0, armor: 0 };
        for (const line of input) {
            const [field, value] = line.split(": ");
            if (field === "Hit Points") {
                stats.hp = parseInt(value);
            } else if (field === "Damage") {
                stats.damage = parseInt(value);
            } else if (field === "Armor") {
                stats.armor = parseInt(value);
            }
        }
        return stats;
    }

    get_best_equipment(player_hp: number, enemy: Stats): [Stats, number] {
        let weapons: Equipment[] = parse_table(weapon_table);
        let armors: Equipment[] = parse_table(armor_table);
        let rings: Equipment[] = parse_table(rings_table);
        let min_cost = Infinity;
        let best_stats: Stats = { hp: 0, damage: 0, armor: 0 };

        for (let weapon_ix = 0; weapon_ix < weapons.length; weapon_ix++) {
            for (let armor_ix = 0; armor_ix < armors.length; armor_ix++) {
                for (let ring1_ix = 0; ring1_ix < rings.length; ring1_ix++) {
                    for (let ring2_ix = 0; ring2_ix < rings.length; ring2_ix++) {
                        if (ring1_ix === ring2_ix && ring1_ix > 0 && ring2_ix > 0) {
                            continue;
                        }
                        const stats: Stats = {
                            hp: player_hp,
                            damage: weapons[weapon_ix].damage + rings[ring1_ix].damage + rings[ring2_ix].damage,
                            armor: armors[armor_ix].armor + rings[ring1_ix].armor + rings[ring2_ix].armor,
                        };
                        const cost =
                            weapons[weapon_ix].cost +
                            armors[armor_ix].cost +
                            rings[ring1_ix].cost +
                            rings[ring2_ix].cost;
                        if (cost < min_cost && this.simulate(stats, Object.assign({}, enemy))) {
                            best_stats = stats;
                            min_cost = cost;
                        }
                    }
                }
            }
        }
        return [best_stats, min_cost];
    }

    get_worst_equipment(player_hp: number, enemy: Stats): [Stats, number] {
        let weapons: Equipment[] = parse_table(weapon_table);
        let armors: Equipment[] = parse_table(armor_table);
        let rings: Equipment[] = parse_table(rings_table);
        let max_cost = 0;
        let worst_stats: Stats = { hp: 0, damage: 0, armor: 0 };

        for (let weapon_ix = 0; weapon_ix < weapons.length; weapon_ix++) {
            for (let armor_ix = 0; armor_ix < armors.length; armor_ix++) {
                for (let ring1_ix = 0; ring1_ix < rings.length; ring1_ix++) {
                    for (let ring2_ix = 0; ring2_ix < rings.length; ring2_ix++) {
                        if (ring1_ix === ring2_ix && ring1_ix > 0 && ring2_ix > 0) {
                            continue;
                        }
                        const stats: Stats = {
                            hp: player_hp,
                            damage: weapons[weapon_ix].damage + rings[ring1_ix].damage + rings[ring2_ix].damage,
                            armor: armors[armor_ix].armor + rings[ring1_ix].armor + rings[ring2_ix].armor,
                        };
                        const cost =
                            weapons[weapon_ix].cost +
                            armors[armor_ix].cost +
                            rings[ring1_ix].cost +
                            rings[ring2_ix].cost;
                        if (cost > max_cost && !this.simulate(stats, Object.assign({}, enemy))) {
                            worst_stats = stats;
                            max_cost = cost;
                        }
                    }
                }
            }
        }
        return [worst_stats, max_cost];
    }

    get_damage_dealt(damage: number, armor: number) {
        const d = damage - armor;
        if (d > 1) {
            return d;
        }
        return 1;
    }

    simulate(player: Stats, enemy: Stats): boolean {
        const player_damage = this.get_damage_dealt(player.damage, enemy.armor);
        const enemy_damage = this.get_damage_dealt(enemy.damage, player.armor);
        let player_turn = true;
        while (player.hp > 0 && enemy.hp > 0) {
            if (player_turn) {
                enemy.hp -= player_damage;
            } else {
                player.hp -= enemy_damage;
            }
            player_turn = !player_turn;
        }

        return player.hp > 0;
    }

    tests(): boolean {
        let player: Stats = { hp: 8, damage: 5, armor: 5 };
        let enemy: Stats = { hp: 12, damage: 7, armor: 2 };
        return this.simulate(player, enemy);
    }

    part1(input: string): number {
        const enemy = this.parse_enemy_stats(input.split("\n"));
        const [_, cost] = this.get_best_equipment(100, enemy);
        return cost;
    }

    part2(input: string): number {
        const enemy = this.parse_enemy_stats(input.split("\n"));
        const [_, cost] = this.get_worst_equipment(100, enemy);
        return cost;
    }
}
