import { Day, run_tests } from "./base";

export class Day20 extends Day {
    nday = 20;

    get_presents(house: number, max_houses: number = -1): number {
        let elfs: number[] = [];
        for (let i = 1; i <= Math.floor(Math.sqrt(house)); i++) {
            if (house % i === 0) {
                elfs.push(i);
            }
        }

        let presents = 0;
        for (const e of elfs) {
            if (max_houses === -1 || house <= e * max_houses) {
                presents += e;
            }
            const e2 = house / e;
            if (max_houses === -1 || house <= e2 * max_houses) {
                if (e !== e2) {
                    presents += e2;
                }
            }
        }

        if (max_houses === -1) {
            return presents * 10;
        } else {
            return presents * 11;
        }
    }

    lowest_house_number(expected_presents: number, max_houses: number = -1): number {
        let house = 0;
        let presents = 0;
        while (presents < expected_presents) {
            house++;
            presents = this.get_presents(house, max_houses);
        }
        return house;
    }

    tests(): boolean {
        let test_inputs = [5, 6, 7, 8, 9, 120];
        let test_results = [60, 120, 80, 150, 130, 3600];

        if (!run_tests(test_inputs, test_results, this.get_presents)) {
            return false;
        }

        test_inputs = [5, 6, 7, 8, 9, 120];
        test_results = [66, 132, 88, 165, 143, 3927];

        if (!run_tests(test_inputs, test_results, (input) => this.get_presents(input, 50))) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        return this.lowest_house_number(parseInt(input));
    }

    part2(input: string): number {
        return this.lowest_house_number(parseInt(input), 50);
    }
}
