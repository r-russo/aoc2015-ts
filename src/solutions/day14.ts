import { Day } from "./base";

interface Reindeer {
    name: string;
    speed: number;
    fly_time: number;
    rest_time: number;
}

export class Day14 extends Day {
    nday = 14;

    parse_input(input: string[]): Reindeer[] {
        let reindeers: Reindeer[] = [];
        for (const line of input) {
            const words = line.split(" ");
            const r: Reindeer = {
                name: words[0],
                speed: parseInt(words[3]),
                fly_time: parseInt(words[6]),
                rest_time: parseInt(words[13]),
            };
            reindeers.push(r);
        }

        return reindeers;
    }

    get_position(time: number, reindeer: Reindeer) {
        const cycle_time = reindeer.fly_time + reindeer.rest_time;
        const num_cycles = Math.floor(time / cycle_time);
        const remainder_time = time % cycle_time;

        return (
            num_cycles * reindeer.speed * reindeer.fly_time +
            reindeer.speed * Math.min(remainder_time, reindeer.fly_time)
        );
    }

    winner_distance_traveled(time: number, reindeers: Reindeer[]): number {
        let best_distance = 0;
        for (const reindeer of reindeers) {
            best_distance = Math.max(best_distance, this.get_position(time, reindeer));
        }
        return best_distance;
    }

    winner_new_rule(time: number, reindeers: Reindeer[]): number {
        let scores = Array(reindeers.length).fill(0);
        for (let i = 1; i <= time; i++) {
            let distances = reindeers.map((r: Reindeer) => this.get_position(i, r));
            const max_distance = Math.max(...distances);
            for (let j = 0; j < reindeers.length; j++) {
                if (distances[j] === max_distance) {
                    scores[j] += 1;
                }
            }
        }

        return Math.max(...scores);
    }

    tests(): boolean {
        let test_input = [
            "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.",
            "Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.",
        ];

        const reindeers = this.parse_input(test_input);
        if (this.winner_distance_traveled(1000, reindeers) !== 1120) {
            return false;
        }

        if (this.winner_new_rule(1000, reindeers) !== 689) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        const reindeers = this.parse_input(input.split("\n"));
        return this.winner_distance_traveled(2503, reindeers);
    }

    part2(input: string): number {
        const reindeers = this.parse_input(input.split("\n"));
        return this.winner_new_rule(2503, reindeers);
    }
}
