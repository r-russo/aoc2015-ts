import { Day } from "./base";

interface Ingredient {
    name: string;
    capacity: number;
    durability: number;
    flavor: number;
    texture: number;
    calories: number;
}

export class Day15 extends Day {
    nday = 15;

    parse_input(input: string[]): Ingredient[] {
        let ingredients: Ingredient[] = [];
        for (const line of input) {
            const words = line.split(" ");
            const ingredient: Ingredient = {
                name: words[0],
                capacity: parseInt(words[2]),
                durability: parseInt(words[4]),
                flavor: parseInt(words[6]),
                texture: parseInt(words[8]),
                calories: parseInt(words[10]),
            };
            ingredients.push(ingredient);
        }

        return ingredients;
    }

    get_score(teaspoons: number[], ingredients: Ingredient[]): number {
        if (teaspoons.length !== ingredients.length) {
            throw "Mismatching lengths";
        }

        let score = 1;

        for (const property in ingredients[0]) {
            if (property === "name" || property === "calories") {
                continue;
            }
            let current_score = 0;
            for (let i = 0; i < teaspoons.length; i++) {
                current_score += teaspoons[i] * ingredients[i][property];
            }
            if (current_score <= 0) {
                return 0;
            }
            score *= current_score;
        }

        return score;
    }

    optimal_teaspons2(ingredients: Ingredient[], count_calories: boolean = false): number {
        let best_score: number = 0;
        for (let i = 1; i < 100; i++) {
            const constrained = 100 - i;
            if (constrained <= 0) {
                continue;
            }
            if (count_calories && ingredients[0].calories * i + ingredients[1].calories * constrained !== 500) {
                continue;
            }
            best_score = Math.max(this.get_score([i, constrained], ingredients), best_score);
        }

        return best_score;
    }

    optimal_teaspons4(ingredients: Ingredient[], count_calories: boolean = false): number {
        let best_score: number = 0;
        for (let i = 1; i < 100; i++) {
            for (let j = 1; j < 100; j++) {
                for (let k = 1; k < 100; k++) {
                    const constrained = 100 - i - j - k;
                    if (constrained <= 0) {
                        continue;
                    }
                    if (
                        count_calories &&
                        ingredients[0].calories * i +
                            ingredients[1].calories * j +
                            ingredients[2].calories * k +
                            ingredients[3].calories * constrained !==
                            500
                    ) {
                        continue;
                    }
                    best_score = Math.max(this.get_score([i, j, k, constrained], ingredients), best_score);
                }
            }
        }

        return best_score;
    }

    tests(): boolean {
        let test_input = [
            "Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8",
            "Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3",
        ];
        let test_result = 62842880;
        const ingredients = this.parse_input(test_input);

        if (this.optimal_teaspons2(ingredients) !== test_result) {
            return false;
        }

        test_result = 57600000;
        if (this.optimal_teaspons2(ingredients, true) !== test_result) {
            return false;
        }

        return true;
    }

    part(input: string): number {
        const ingredients = this.parse_input(input.split("\n"));
        return this.optimal_teaspons4(ingredients);
    }

    part2(input: string): number {
        const ingredients = this.parse_input(input.split("\n"));
        return this.optimal_teaspons4(ingredients, true);
    }
}
