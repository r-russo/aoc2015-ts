import { Day, run_tests } from "./base";
import md5 from "crypto-js/md5";

export class Day4 extends Day {
    nday = 4;

    find_hash(key: string, leading_zeros: number = 5) {
        let result = -1;
        for (let i = 0; i < 10 ** (key.length + 2); i++) {
            const hash = md5(key + i.toString(10)).toString();
            if (hash.substr(0, leading_zeros) === "0".repeat(leading_zeros)) {
                result = i;
                break;
            }
        }

        return result;
    }

    tests(): boolean {
        let test_inputs = ["abcdef", "pqrstuv"];
        let test_results = [609043, 1048970];

        if (!run_tests(test_inputs, test_results, this.find_hash)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        const result = this.find_hash(input);

        return result;
    }

    part2(input: string): number {
        const result = this.find_hash(input, 6);

        return result;
    }
}
