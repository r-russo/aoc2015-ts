import { Day } from "./base";

interface Registers {
    [index: string]: number;
}

const get_operand = (s: string, r: Registers): number | undefined => {
    const n = parseInt(s);
    if (isNaN(n)) {
        return r[s];
    } else {
        return n;
    }
};

const do_operation = (operation: string, op1: number, op2: number | undefined = undefined): number => {
    switch (operation) {
        case "AND":
            return op1 & op2;
        case "OR":
            return op1 | op2;
        case "LSHIFT":
            return (op1 << op2) & 0xffff;
        case "RSHIFT":
            return (op1 >>> op2) & 0xffff;
        case "NOT":
            return ~op1 & 0xffff;
    }
};

export class Day7 extends Day {
    nday = 7;

    parse(instruction: string, registers: Registers): boolean {
        const e = instruction.split(" ");

        if (instruction.match(/^[0-9a-z]+ -> [a-z]+$/)) {
            const operand = get_operand(e[0], registers);
            if (typeof operand === "undefined") {
                return false;
            }
            registers[e[2]] = get_operand(e[0], registers);
        } else if (instruction.match(/^[a-z0-9]+ [A-Z]+ [a-z0-9]+ -> [a-z]+$/)) {
            let operand1 = get_operand(e[0], registers);
            if (typeof operand1 === "undefined") {
                return false;
            }
            let operand2 = get_operand(e[2], registers);
            if (typeof operand2 === "undefined") {
                return false;
            }

            registers[e[4]] = do_operation(e[1], operand1, operand2);
        } else if (instruction.match(/^[A-Z]+ [a-z0-9]+ -> [a-z]+$/)) {
            let operand = get_operand(e[1], registers);
            if (typeof operand === "undefined") {
                return false;
            }
            registers[e[3]] = do_operation(e[0], operand);
        }

        return true;
    }

    process_input(input: string[]): Registers {
        let r: Registers = {};
        let q = input.slice();
        while (q.length > 0) {
            const instruction = q.shift();
            if (!this.parse(instruction, r)) {
                q.push(instruction);
            }
        }

        return r;
    }

    tests(): boolean {
        let test_input = [
            "123 -> x",
            "456 -> y",
            "x AND y -> d",
            "x OR y -> e",
            "x LSHIFT 2 -> f",
            "y RSHIFT 2 -> g",
            "NOT x -> h",
            "NOT y -> i",
        ];
        let test_result = { x: 123, y: 456, d: 72, e: 507, f: 492, g: 114, h: 65412, i: 65079 };

        let r = this.process_input(test_input);

        if (JSON.stringify(test_result) !== JSON.stringify(r)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        let r = this.process_input(input.split("\n"));
        return r.a;
    }

    part2(input: string): number {
        let instructions = input.split("\n");
        let r = this.process_input(instructions);

        for (let i = 0; i < instructions.length; i++) {
            if (instructions[i].match(/^[0-9a-z]+ -> b$/)) {
                instructions[i] = `${r.a} -> b`;
                break;
            }
        }

        r = this.process_input(instructions);
        return r.a;
    }
}
