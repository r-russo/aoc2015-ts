import { Day } from "./base";

interface Stats {
    hp: number;
    damage?: number;
    mana?: number;
}

export class Day22 extends Day {
    nday = 22;

    parse_enemy_stats(input: string[]): Stats {
        let stats: Stats = { hp: 0, damage: 0 };
        for (const line of input) {
            const [field, value] = line.split(": ");
            if (field === "Hit Points") {
                stats.hp = parseInt(value);
            } else if (field === "Damage") {
                stats.damage = parseInt(value);
            }
        }
        return stats;
    }

    get_damage_dealt(damage: number, armor: number) {
        const d = damage - armor;
        if (d > 1) {
            return d;
        }
        return 1;
    }

    simulate(player: Stats, enemy: Stats, spells: string[], hardmode: boolean = false): [number, number] {
        let timers: { [spell: string]: number } = {};
        let spells_cost = {
            "Magic Missile": 53,
            Drain: 73,
            Shield: 113,
            Poison: 173,
            Recharge: 229,
        };
        let player_turn = true;
        let total_cost = 0;
        while (player.hp > 0 && enemy.hp > 0) {
            if (hardmode) {
                player.hp--;
                if (player.hp <= 0) {
                    break;
                }
            }
            let bonus_armor = 0;
            for (const spell in timers) {
                if (typeof timers[spell] === "undefined" || timers[spell] === 0) {
                    continue;
                }

                if (spell === "Shield") {
                    bonus_armor += 7;
                } else if (spell === "Poison") {
                    enemy.hp -= 3;
                } else if (spell === "Recharge") {
                    player.mana += 101;
                }

                timers[spell]--;
                if (timers[spell] < 0) {
                    timers[spell] = 0;
                }
            }
            if (enemy.hp <= 0) {
                break;
            }

            const enemy_damage = this.get_damage_dealt(enemy.damage, bonus_armor);
            if (player_turn) {
                const current_spell = spells.shift();

                if (typeof current_spell === "undefined") {
                    return [0, total_cost];
                }

                let player_damage = 0;
                if (current_spell === "Magic Missile") {
                    player_damage = 4;
                } else if (current_spell === "Drain") {
                    player_damage = 2;
                    player.hp += 2;
                } else if (current_spell === "Shield") {
                    if (typeof timers["Shield"] === "undefined" || timers["Shield"] === 0) {
                        timers["Shield"] = 6;
                    } else {
                        return [-1, Infinity];
                    }
                } else if (current_spell === "Poison") {
                    if (typeof timers["Poison"] === "undefined" || timers["Poison"] === 0) {
                        timers["Poison"] = 6;
                    } else {
                        return [-1, Infinity];
                    }
                } else if (current_spell === "Recharge") {
                    if (typeof timers["Recharge"] === "undefined" || timers["Recharge"] === 0) {
                        timers["Recharge"] = 5;
                    } else {
                        return [-1, Infinity];
                    }
                }
                total_cost += spells_cost[current_spell];
                player.mana -= spells_cost[current_spell];
                if (player.mana < 0) {
                    return [-1, total_cost];
                }

                enemy.hp -= player_damage;
            } else {
                player.hp -= enemy_damage;
            }
            player_turn = !player_turn;
        }

        if (player.hp > 0) {
            return [1, total_cost];
        }
        return [-1, total_cost];
    }

    find_best_spells(player: Stats, enemy: Stats, hardmode: boolean = false) {
        let stack: string[][] = [];

        const list_spells = ["Magic Missile", "Drain", "Shield", "Poison", "Recharge"];
        for (const spell of list_spells) {
            stack.push([spell]);
        }

        let min_cost = Infinity;

        while (stack.length > 0) {
            const spells = stack.pop();
            const [winner, cost] = this.simulate(
                { hp: player.hp, mana: player.mana },
                { hp: enemy.hp, damage: enemy.damage },
                spells.slice(),
                hardmode
            );

            if (winner === 0) {
                if (cost < min_cost) {
                    for (const spell of list_spells) {
                        stack.push(spells.concat(spell));
                    }
                }
            } else if (winner === 1) {
                if (cost < min_cost) {
                    min_cost = cost;
                }
            }
        }

        return min_cost;
    }

    tests() {
        let player: Stats = { hp: 10, mana: 250 };
        let enemy: Stats = { hp: 13, damage: 8 };
        if (this.simulate(player, enemy, ["Poison", "Magic Missile"])[0] !== 1) {
            return false;
        }

        player = { hp: 10, mana: 250 };
        enemy = { hp: 14, damage: 8 };
        if (this.simulate(player, enemy, ["Recharge", "Shield", "Drain", "Poison", "Magic Missile"])[0] !== 1) {
            return false;
        }

        console.log(
            this.simulate({ hp: 50, mana: 500 }, { hp: 58, damage: 9 }, [
                "Poison",
                "Shield",
                "Drain",
                "Poison",
                "Magic Missile",
                "Magic Missile",
                "Magic Missile",
                "Magic Missile",
                "Magic Missile",
            ])
        );

        return true;
    }

    part1(input: string): number {
        const player: Stats = { hp: 50, mana: 500 };
        const enemy: Stats = this.parse_enemy_stats(input.split("\n"));
        return this.find_best_spells(player, enemy);
    }

    part2(input: string): number {
        const player: Stats = { hp: 50, mana: 500 };
        const enemy: Stats = this.parse_enemy_stats(input.split("\n"));
        return this.find_best_spells(player, enemy, true);
    }
}
