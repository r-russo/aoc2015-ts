export abstract class Day {
    abstract nday: number;
    abstract tests(): boolean;
    abstract part1(input: string): number | string;
    abstract part2(input: string): number | string;
}

export function run_tests<T, U>(inputs: U[], true_value: T[], func: (input: U) => T): boolean {
    for (let i = 0; i < inputs.length; i++) {
        const result = func(inputs[i]);
        if (result !== true_value[i]) {
            console.log(`Test failed at input: ${inputs[i]} with result ${result}. Should return ${true_value[i]}`);
            return false;
        }
    }
    return true;
}

export function combinations_without_repetition<T>(list: T[], k: number): T[][] {
    let ret: T[][] = [];
    for (let i = 0; i < list.length; i++) {
        if (k === 1) {
            ret.push([list[i]]);
        } else {
            const sub = combinations_without_repetition(list.slice(i + 1, list.length), k - 1);
            for (let j = 0; j < sub.length; j++) {
                const next = sub[j];
                next.unshift(list[i]);
                ret.push(next);
            }
        }
    }
    return ret;
}
