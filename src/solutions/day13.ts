import { Day } from "./base";

interface PointsTable {
    [person: string]: { [next_to: string]: number };
}

export class Day13 extends Day {
    nday = 13;

    parse_input(input: string[]): PointsTable {
        let points_table: PointsTable = {};
        for (const line of input) {
            const words = line.split(" ");
            const person = words[0];
            const next_to = words[10].substr(0, words[10].length - 1);
            let points: number = parseInt(words[3]);
            if (words[2] === "lose") {
                points *= -1;
            }
            if (typeof points_table[person] === "undefined") {
                points_table[person] = {};
            }
            points_table[person][next_to] = points;
        }

        return points_table;
    }

    total_happiness(positions: string[], points_table: PointsTable): number {
        let total_points = 0;
        for (let i = 0; i < positions.length; i++) {
            const person = positions[i];

            let n1: string;
            if (i - 1 === -1) {
                n1 = positions[positions.length - 1];
            } else {
                n1 = positions[i - 1];
            }

            let n2: string;
            if (i + 1 === positions.length) {
                n2 = positions[0];
            } else {
                n2 = positions[i + 1];
            }

            total_points += points_table[person][n1];
            total_points += points_table[person][n2];
        }

        return total_points;
    }

    get_permutations(k: number, positions: string[], permutations: string[][]) {
        if (k === 1) {
            permutations.push(positions.slice());
        } else {
            this.get_permutations(k - 1, positions, permutations);

            for (let i = 0; i < k - 1; i++) {
                if (k % 2 === 0) {
                    const tmp = positions[i];
                    positions[i] = positions[k - 1];
                    positions[k - 1] = tmp;
                } else {
                    const tmp = positions[0];
                    positions[0] = positions[k - 1];
                    positions[k - 1] = tmp;
                }
                this.get_permutations(k - 1, positions, permutations);
            }
        }
    }

    find_optimal(points_table: PointsTable): number {
        let people: string[] = [];
        for (const person in points_table) {
            people.push(person);
        }

        let permutations: string[][] = [];
        this.get_permutations(people.length, people, permutations);

        let best_score = 0;
        for (const p of permutations) {
            const score = this.total_happiness(p, points_table);
            best_score = Math.max(score, best_score);
        }

        return best_score;
    }

    tests(): boolean {
        let test_input = [
            "Alice would gain 54 happiness units by sitting next to Bob.",
            "Alice would lose 79 happiness units by sitting next to Carol.",
            "Alice would lose 2 happiness units by sitting next to David.",
            "Bob would gain 83 happiness units by sitting next to Alice.",
            "Bob would lose 7 happiness units by sitting next to Carol.",
            "Bob would lose 63 happiness units by sitting next to David.",
            "Carol would lose 62 happiness units by sitting next to Alice.",
            "Carol would gain 60 happiness units by sitting next to Bob.",
            "Carol would gain 55 happiness units by sitting next to David.",
            "David would gain 46 happiness units by sitting next to Alice.",
            "David would lose 7 happiness units by sitting next to Bob.",
            "David would gain 41 happiness units by sitting next to Carol.",
        ];

        const points_table = this.parse_input(test_input);

        if (this.find_optimal(points_table) !== 330) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        const points_table = this.parse_input(input.split("\n"));
        return this.find_optimal(points_table);
    }

    part2(input: string): number {
        let points_table = this.parse_input(input.split("\n"));
        points_table["Me"] = {};
        for (const person in points_table) {
            points_table["Me"][person] = 0;
            points_table[person]["Me"] = 0;
        }
        return this.find_optimal(points_table);
    }
}
