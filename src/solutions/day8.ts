import { Day, run_tests } from "./base";

export class Day8 extends Day {
    nday = 8;

    count_characters(line: string): number {
        line = line.substr(1, line.length - 2);
        let count = 0;
        let i = 0;
        while (i < line.length) {
            if (line[i] === "\\") {
                if (line[i + 1] !== "x") {
                    i++;
                } else {
                    i += 3;
                }
            }
            count++;
            i++;
        }

        return count;
    }

    count_encoded(line: string): number {
        let count = 0;
        for (const c of line) {
            if (c === '"' || c === "\\") {
                count++;
            }
            count++;
        }

        return count + 2;
    }

    tests(): boolean {
        let test_inputs = [String.raw`""`, String.raw`"abc"`, String.raw`"aaa\"aaa"`, String.raw`"\x27"`];
        let test_results = [0, 3, 7, 1];

        if (!run_tests(test_inputs, test_results, this.count_characters)) {
            return false;
        }

        test_inputs = [String.raw`""`, String.raw`"abc"`, String.raw`"aaa\"aaa"`, String.raw`"\x27"`];
        test_results = [6, 9, 16, 11];

        if (!run_tests(test_inputs, test_results, this.count_encoded)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        let result = 0;
        for (const line of input.split("\n")) {
            result += line.length - this.count_characters(line);
        }

        return result;
    }

    part2(input: string): number {
        let result = 0;
        for (const line of input.split("\n")) {
            result += this.count_encoded(line) - line.length;
        }

        return result;
    }
}
