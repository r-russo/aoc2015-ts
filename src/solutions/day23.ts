import { Day } from "./base";

export class Day23 extends Day {
    nday = 23;

    run_program(program: string[], a: number = 0, b: number = 0) {
        let regs = { a: a, b: b };
        let pc = 0;

        while (pc < program.length) {
            const instruction = program[pc];
            pc++;
            const [op, v1, v2] = instruction.split(" ");
            if (op === "hlf") {
                regs[v1] /= 2;
            } else if (op === "tpl") {
                regs[v1] *= 3;
            } else if (op === "inc") {
                regs[v1]++;
            } else if (op === "jmp") {
                pc += parseInt(v1) - 1;
            } else if (op === "jie") {
                if (regs[v1.substr(0, 1)] % 2 === 0) {
                    pc += parseInt(v2) - 1;
                }
            } else if (op === "jio") {
                if (regs[v1.substr(0, 1)] === 1) {
                    pc += parseInt(v2) - 1;
                }
            } else {
                console.log(`Unrecognized instruction: ${instruction}`);
            }
        }

        return regs;
    }

    tests(): boolean {
        let test_input = ["inc a", "jio a, +2", "tpl a", "inc a"];
        let test_result = 2;

        if (this.run_program(test_input).a !== test_result) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        return this.run_program(input.split("\n")).b;
    }

    part2(input: string): number {
        return this.run_program(input.split("\n"), 1).b;
    }
}
