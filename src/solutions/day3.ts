import { Day, run_tests } from "./base";

class Coordinate {
    x: number;
    y: number;
    turn: boolean;
    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    toString(): string {
        return `${this.x},${this.y}`;
    }
}
export class Day3 extends Day {
    nday = 3;

    count_unique_deliveries(directions: string): number {
        let visited_houses: Set<string> = new Set();
        let coord: Coordinate = new Coordinate();
        visited_houses.add(coord.toString());
        for (const c of directions) {
            switch (c) {
                case ">":
                    coord.x += 1;
                    break;
                case "<":
                    coord.x -= 1;
                    break;
                case "^":
                    coord.y -= 1;
                    break;
                case "v":
                    coord.y += 1;
                    break;
            }

            const coord_str = coord.toString();
            if (!visited_houses.has(coord_str)) {
                visited_houses.add(coord_str);
            }
        }

        return visited_houses.size;
    }

    count_deliveries_turns(directions: string): number {
        let visited_houses: Set<string> = new Set();
        let coord_santa: Coordinate = new Coordinate();
        let coord_robosanta: Coordinate = new Coordinate();
        visited_houses.add(coord_santa.toString());
        let turn_santa = true;
        for (const c of directions) {
            let current_coord: Coordinate;
            if (turn_santa) {
                current_coord = coord_santa;
            } else {
                current_coord = coord_robosanta;
            }
            switch (c) {
                case ">":
                    current_coord.x += 1;
                    break;
                case "<":
                    current_coord.x -= 1;
                    break;
                case "^":
                    current_coord.y -= 1;
                    break;
                case "v":
                    current_coord.y += 1;
                    break;
            }

            const coord_str = current_coord.toString();
            if (!visited_houses.has(coord_str)) {
                visited_houses.add(coord_str);
            }

            turn_santa = !turn_santa;
        }

        return visited_houses.size;
    }

    tests(): boolean {
        let test_inputs = [">", "^>v<", "^v^v^v^v^v"];
        let test_results = [2, 4, 2];
        if (!run_tests(test_inputs, test_results, this.count_unique_deliveries)) {
            return false;
        }

        test_inputs = ["^v", "^>v<", "^v^v^v^v^v"];
        test_results = [3, 3, 11];
        if (!run_tests(test_inputs, test_results, this.count_deliveries_turns)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        let result = this.count_unique_deliveries(input);

        return result;
    }

    part2(input: string): number {
        let result = this.count_deliveries_turns(input);

        return result;
    }
}
