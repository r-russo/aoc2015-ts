import { Day, combinations_without_repetition } from "./base";

export class Day17 extends Day {
    nday = 17;

    valid_combinations(containers: number[], capacity: number): number[][] {
        let combinations: number[][] = [];
        for (let i = 1; i < containers.length; i++) {
            for (const combination of combinations_without_repetition(containers, i)) {
                if (combination.reduce((r, x) => r + x, 0) === capacity) {
                    combinations.push(combination);
                }
            }
        }
        return combinations;
    }

    count_combinations_min_length(containers: number[], capacity: number): number {
        const combinations = this.valid_combinations(containers, capacity);
        const min_length = combinations.reduce((r, x) => Math.min(r, x.length), Infinity);

        let count = 0;
        for (const combination of combinations) {
            if (combination.length === min_length) {
                count++;
            }
        }
        return count;
    }

    tests(): boolean {
        const containers = [20, 15, 10, 5, 5];
        if (this.valid_combinations(containers, 25).length !== 4) {
            return false;
        }

        if (this.count_combinations_min_length(containers, 25) !== 3) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        const containers = input.split("\n").map((x) => parseInt(x));
        return this.valid_combinations(containers, 150).length;
    }

    part2(input: string): number {
        const containers = input.split("\n").map((x) => parseInt(x));
        return this.count_combinations_min_length(containers, 150);
    }
}
