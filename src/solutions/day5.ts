import { Day, run_tests } from "./base";

export class Day5 extends Day {
    nday = 5;

    is_nice(word: string): boolean {
        let count_vowels = 0;
        let repeating_letter = false;
        let forbidden_pattern = false;
        let last_letter = "";
        for (const c of word) {
            if ("aeiou".includes(c)) {
                count_vowels += 1;
            }

            if (c === last_letter) {
                repeating_letter = true;
            }

            if (["ab", "cd", "pq", "xy"].includes(last_letter + c)) {
                forbidden_pattern = true;
                break;
            }

            last_letter = c;
        }

        return count_vowels >= 3 && repeating_letter && !forbidden_pattern;
    }

    is_nice2(word: string): boolean {
        let pairs: Set<string> = new Set();
        let last_two_letters = word.substr(0, 2);
        let found_pair = false;
        let repeat_after = false;

        pairs.add(last_two_letters);
        for (let i = 2; i < word.length; i++) {
            if (pairs.has(word.substr(i, 2))) {
                found_pair = true;
            }

            if (word[i] === last_two_letters[0]) {
                repeat_after = true;
            }

            last_two_letters = word.substr(i - 1, 2);
            pairs.add(last_two_letters);
        }

        return found_pair && repeat_after;
    }

    tests(): boolean {
        let test_inputs = ["ugknbfddgicrmopn", "aaa", "jchzalrnumimnmhp", "haegwjzuvuyypxyu", "dvszwmarrgswjxmb"];
        let test_results = [true, true, false, false, false];

        if (!run_tests(test_inputs, test_results, this.is_nice)) {
            return false;
        }

        test_inputs = ["qjhvhtzxzqqjkmpb", "xxyxx", "uurcxstgmygtbstg", "ieodomkazucvgmuy"];
        test_results = [true, true, false, false];

        if (!run_tests(test_inputs, test_results, this.is_nice2)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        let result = 0;
        for (const line of input.split("\n")) {
            if (this.is_nice(line)) {
                result += 1;
            }
        }

        return result;
    }

    part2(input: string): number {
        let result = 0;
        for (const line of input.split("\n")) {
            if (this.is_nice2(line)) {
                result += 1;
            }
        }

        return result;
    }
}
