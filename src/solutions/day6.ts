import { Day, run_tests } from "./base";

class Grid {
    values: number[];
    size: number;
    constructor(size: number = 1000) {
        this.values = Array(size * size).fill(0);
        this.size = size;
    }

    turn_lights(from: number[], to: number[], command: string) {
        for (let x = from[0]; x <= to[0]; x++) {
            for (let y = from[1]; y <= to[1]; y++) {
                switch (command) {
                    case "turn on":
                        this.values[y * this.size + x] = 1;
                        break;
                    case "turn off":
                        this.values[y * this.size + x] = 0;
                        break;
                    case "toggle":
                        this.values[y * this.size + x] = this.values[y * this.size + x] > 0 ? 0 : 1;
                        break;
                }
            }
        }
    }

    turn_lights2(from: number[], to: number[], command: string) {
        for (let x = from[0]; x <= to[0]; x++) {
            for (let y = from[1]; y <= to[1]; y++) {
                switch (command) {
                    case "turn on":
                        this.values[y * this.size + x] += 1;
                        break;
                    case "turn off":
                        this.values[y * this.size + x] = Math.max(0, this.values[y * this.size + x] - 1);
                        break;
                    case "toggle":
                        this.values[y * this.size + x] += 2;
                        break;
                }
            }
        }
    }

    parse_instruction(instruction: string, part1: boolean = true) {
        const command = instruction.match(/turn on|toggle|turn off/)[0];
        const coordinates = instruction.match(/[0-9]+,[0-9]+/g);
        const from = coordinates[0].split(",").map((x: string) => parseInt(x));
        const to = coordinates[1].split(",").map((x: string) => parseInt(x));
        if (part1) {
            this.turn_lights(from, to, command);
        } else {
            this.turn_lights2(from, to, command);
        }
    }

    count_brightness() {
        return this.values.reduce((v, r) => v + r, 0);
    }
}

export class Day6 extends Day {
    nday = 6;

    tests(): boolean {
        let test_inputs = [
            "turn on 0,0 through 999,999",
            "toggle 0,0 through 999,0",
            "turn off 499,499 through 500,500",
        ];
        let test_results = [1000000, 1000, 0];
        if (
            !run_tests(test_inputs, test_results, (input: string): number => {
                const g = new Grid();
                g.parse_instruction(input);
                return g.count_brightness();
            })
        ) {
            return false;
        }

        test_inputs = ["turn on 0,0 through 0,0", "toggle 0,0 through 999,999"];
        test_results = [1, 2000000];
        if (
            !run_tests(test_inputs, test_results, (input: string): number => {
                const g = new Grid();
                g.parse_instruction(input, false);
                return g.count_brightness();
            })
        ) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        const grid = new Grid();
        for (const instruction of input.split("\n")) {
            grid.parse_instruction(instruction);
        }
        return grid.count_brightness();
    }

    part2(input: string): number {
        const grid = new Grid();
        for (const instruction of input.split("\n")) {
            grid.parse_instruction(instruction, false);
        }
        return grid.count_brightness();
    }
}
