import { Day } from "./base";

class Board {
    w: number;
    h: number;
    tiles: string[];
    stuck: boolean;

    constructor(input: string[], stuck: boolean = false) {
        this.h = input.length;
        this.w = input[0].length;
        this.tiles = [];
        this.stuck = stuck;

        for (const line of input) {
            this.tiles.push(...line.split(""));
        }

        if (stuck) {
            this.tiles[0] = "#";
            this.tiles[this.w - 1] = "#";
            this.tiles[(this.h - 1) * this.w] = "#";
            this.tiles[this.h * this.w - 1] = "#";
        }
    }

    get_tile(x: number, y: number): string {
        if (x < 0 || x >= this.w || y < 0 || y >= this.h) {
            return ".";
        }
        return this.tiles[y * this.w + x];
    }

    get_neighbours(x: number, y: number): string[] {
        let n: string[] = [];

        n.push(this.get_tile(x + 1, y));
        n.push(this.get_tile(x - 1, y));
        n.push(this.get_tile(x + 1, y + 1));
        n.push(this.get_tile(x + 1, y - 1));
        n.push(this.get_tile(x - 1, y + 1));
        n.push(this.get_tile(x - 1, y - 1));
        n.push(this.get_tile(x, y + 1));
        n.push(this.get_tile(x, y - 1));

        return n;
    }

    step() {
        let new_state = this.tiles.slice();

        for (let x = 0; x < this.w; x++) {
            for (let y = 0; y < this.h; y++) {
                if (this.stuck && (x === 0 || x === this.w - 1) && (y === 0 || y === this.h - 1)) {
                    continue;
                }
                const tile = this.get_tile(x, y);
                let count_on = 0;
                for (const n of this.get_neighbours(x, y)) {
                    if (n === "#") {
                        count_on++;
                    }
                }

                if (tile === "#" && (count_on < 2 || count_on > 3)) {
                    new_state[y * this.w + x] = ".";
                } else if (tile === "." && count_on === 3) {
                    new_state[y * this.w + x] = "#";
                }
            }
        }

        this.tiles = new_state;
    }

    simulate(steps: number) {
        for (let i = 0; i < steps; i++) {
            this.step();
        }
    }

    count_lights_on(): number {
        let count = 0;
        for (const tile of this.tiles) {
            if (tile === "#") {
                count++;
            }
        }

        return count;
    }
}

export class Day18 extends Day {
    nday = 18;

    tests(): boolean {
        let test_input = [".#.#.#", "...##.", "#....#", "..#...", "#.#..#", "####.."];
        let board = new Board(test_input);

        board.simulate(4);
        if (board.count_lights_on() !== 4) {
            return false;
        }

        board = new Board(test_input, true);
        board.simulate(5);
        if (board.count_lights_on() !== 17) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        let board = new Board(input.split("\n"));
        board.simulate(100);

        return board.count_lights_on();
    }

    part2(input: string): number {
        let board = new Board(input.split("\n"), true);
        board.simulate(100);

        return board.count_lights_on();
    }
}
