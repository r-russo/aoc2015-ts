import { Day } from "./base";

interface Materials {
    children: number;
    cats: number;
    samoyeds: number;
    pomeranians: number;
    akitas: number;
    vizslas: number;
    goldfish: number;
    trees: number;
    cars: number;
    perfumes: number;
}

export class Day16 extends Day {
    nday = 16;

    parse_input(input: string[]): Materials[] {
        let materials: Materials[] = [];

        for (const line of input) {
            let aunt: Materials = {
                children: -1,
                cats: -1,
                samoyeds: -1,
                pomeranians: -1,
                akitas: -1,
                vizslas: -1,
                goldfish: -1,
                trees: -1,
                cars: -1,
                perfumes: -1,
            };

            const sep_ix = line.indexOf(":") + 2;
            const list = line.substr(sep_ix).split(", ");
            for (const item of list) {
                const [material, count] = item.split(": ");
                aunt[material] = parseInt(count);
            }
            materials.push(aunt);
        }

        return materials;
    }

    find_aunt(materials: Materials[]): number {
        const expected: Materials = {
            children: 3,
            cats: 7,
            samoyeds: 2,
            pomeranians: 3,
            akitas: 0,
            vizslas: 0,
            goldfish: 5,
            trees: 3,
            cars: 2,
            perfumes: 1,
        };

        let best_match = -1;
        for (let i = 0; i < materials.length; i++) {
            let found = true;
            for (const key in materials[i]) {
                if (materials[i][key] !== -1 && materials[i][key] !== expected[key]) {
                    found = false;
                    break;
                }
            }

            if (found) {
                best_match = i + 1;
            }
        }

        return best_match;
    }

    find_aunt2(materials: Materials[]): number {
        const expected: Materials = {
            children: 3,
            cats: 7,
            samoyeds: 2,
            pomeranians: 3,
            akitas: 0,
            vizslas: 0,
            goldfish: 5,
            trees: 3,
            cars: 2,
            perfumes: 1,
        };

        let best_match = -1;
        for (let i = 0; i < materials.length; i++) {
            let found = true;
            for (const key in materials[i]) {
                if (materials[i][key] === -1) {
                    continue;
                }

                if (["cats", "trees"].includes(key)) {
                    found = materials[i][key] > expected[key];
                } else if (["pomeranians", "goldfish"].includes(key)) {
                    found = materials[i][key] < expected[key];
                } else {
                    found = materials[i][key] === expected[key];
                }

                if (!found) {
                    break;
                }
            }

            if (found) {
                best_match = i + 1;
            }
        }

        return best_match;
    }

    tests(): boolean {
        return true;
    }

    part1(input: string): number {
        const materials = this.parse_input(input.split("\n"));
        return this.find_aunt(materials);
    }

    part2(input: string): number {
        const materials = this.parse_input(input.split("\n"));
        return this.find_aunt2(materials);
    }
}
