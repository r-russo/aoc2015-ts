import { Day } from "./base";

export class Day25 extends Day {
    nday = 25;

    table_to_linear(row: number, col: number): number {
        let n = ((col + 1) * col) / 2;
        for (let i = 0; i < row - 1; i++) {
            n += i + col;
        }
        return n;
    }

    get_code(n: number) {
        let result = 20151125;
        for (let i = 0; i < n - 1; i++) {
            result = (result * 252533) % 33554393;
        }
        return result;
    }

    tests(): boolean {
        if (this.get_code(this.table_to_linear(6, 6)) !== 27995004) {
            return false;
        }
        return true;
    }

    part1(input: string): number {
        let row, col;
        const words = input.split(" ");
        for (let i = 0; i < words.length; i++) {
            if (words[i] === "row") {
                row = parseInt(words[i + 1]);
            } else if (words[i] === "column") {
                col = parseInt(words[i + 1]);
            }
        }
        return this.get_code(this.table_to_linear(row, col));
    }

    part2(input: string): string {
        return "DONE";
    }
}
