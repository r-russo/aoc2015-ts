import { Day, run_tests } from "./base";

export class Day2 extends Day {
    nday = 2;

    total_paper(dimensions: string) {
        if (dimensions.length === 0) {
            console.log("Found empty string with input", dimensions, ". Returning 0...");
            return 0;
        }
        const [l, w, h] = dimensions.split("x").map((x) => parseInt(x));
        const areas = [l * w, w * h, l * h];
        const total_surface = areas.reduce((ac, v) => ac + 2 * v, 0);
        const slack = Math.min(...areas);
        return total_surface + slack;
    }

    total_ribbon(dimensions: string): number {
        if (dimensions.length === 0) {
            console.log("Found empty string with input", dimensions, ". Returning 0...");
            return 0;
        }
        const sides = dimensions.split("x").map((x) => parseInt(x));

        const bow = sides.reduce((ac, v) => ac * v);
        const sorted_sides = sides.slice().sort((a, b) => a - b);

        return bow + sorted_sides[0] * 2 + sorted_sides[1] * 2;
    }

    tests(): boolean {
        let test_inputs = ["2x3x4", "1x1x10"];
        let test_results = [58, 43];

        if (!run_tests(test_inputs, test_results, this.total_paper)) {
            return false;
        }

        test_results = [34, 14];
        if (!run_tests(test_inputs, test_results, this.total_ribbon)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        let counter = 0;
        for (const inp of input.split("\n")) {
            counter += this.total_paper(inp);
        }

        return counter;
    }

    part2(input: string): number {
        let counter = 0;
        for (const inp of input.split("\n")) {
            counter += this.total_ribbon(inp);
        }

        return counter;
    }
}
