import { Day, run_tests } from "./base";

export class Day11 extends Day {
    nday = 11;

    check_requirements(password: string): boolean {
        let count_pairs = 0;
        let has_inc_straight = false;
        let pairs: Set<string> = new Set();
        for (let i = 0; i < password.length; i++) {
            const c = password[i];
            if (c === "i" || c === "o" || c === "l") {
                return false;
            }

            if (i > 0 && password[i - 1] === c && !pairs.has(c)) {
                count_pairs++;
                pairs.add(c);
            }

            if (
                i > 1 &&
                !has_inc_straight &&
                password.charCodeAt(i - 2) + 1 === password.charCodeAt(i - 1) &&
                password.charCodeAt(i - 1) + 1 === c.charCodeAt(0)
            ) {
                has_inc_straight = true;
            }
        }

        return has_inc_straight && count_pairs >= 2;
    }

    next_password(password: string) {
        const z_charcode = "z".charCodeAt(0);
        const a_charcode = "a".charCodeAt(0);
        let new_password: string[] = password.split("");

        let carry = 0;
        for (let i = password.length - 1; i >= 0; i--) {
            let n = password.charCodeAt(i) + carry;
            if (i === password.length - 1) {
                n++;
            }
            if (n > z_charcode) {
                n = a_charcode;
                new_password[i] = String.fromCharCode(n);
                carry = 1;
            } else {
                new_password[i] = String.fromCharCode(n);
                break;
            }
        }

        return new_password.join("");
    }

    tests(): boolean {
        let test_inputs = ["hijklmmn", "abbceffg", "abbcegjk"];
        let test_results = [false, false, false];
        if (!run_tests(test_inputs, test_results, this.check_requirements)) {
            return false;
        }

        test_inputs = ["abcdefgh", "ghijklmn"];
        let test_results2 = ["abcdffaa", "ghjaabcc"];
        if (
            !run_tests(test_inputs, test_results2, (password: string) => {
                while (true) {
                    password = this.next_password(password);
                    if (this.check_requirements(password)) {
                        return password;
                    }
                }
            })
        ) {
            return false;
        }

        return true;
    }

    part1(input: string): string {
        let password = input;
        while (true) {
            password = this.next_password(password);
            if (this.check_requirements(password)) {
                return password;
            }
        }
    }

    part2(input: string): string {
        return this.part1(this.part1(input));
    }
}
