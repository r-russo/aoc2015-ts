import { Day, run_tests } from "./base";

export class Day1 extends Day {
    nday = 1;

    parse_parens(input: string): number {
        let counter = 0;
        for (const c of input) {
            if (c == "(") {
                counter++;
            } else if (c == ")") {
                counter--;
            }
        }

        return counter;
    }

    find_first_basement(input: string): number {
        let counter = 0;
        for (let position = 0; position < input.length; position++) {
            if (input[position] == "(") {
                counter++;
            } else if (input[position] == ")") {
                counter--;
            }

            if (counter == -1) {
                return position + 1;
            }
        }

        return -1;
    }

    tests(): boolean {
        let test_inputs: string[] = ["(())", "()()", "(((", "(()(()(", "))(((((", "())", "))(", ")))", ")())())"];
        let test_results: number[] = [0, 0, 3, 3, 3, -1, -1, -3, -3];

        if (!run_tests(test_inputs, test_results, this.parse_parens)) {
            return false;
        }

        test_inputs = [")", "()())"];
        test_results = [1, 5];
        if (!run_tests(test_inputs, test_results, this.find_first_basement)) {
            return false;
        }

        return true;
    }

    part1(input: string): number {
        const result = this.parse_parens(input);
        return result;
    }

    part2(input: string): number {
        const result = this.find_first_basement(input);
        return result;
    }
}
