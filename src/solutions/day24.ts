import { Day, combinations_without_repetition } from "./base";

export class Day24 extends Day {
    nday = 24;

    find_min_qe(input: number[], n_groups: number = 3) {
        const total_weight = input.reduce((r, x) => r + x, 0);
        const group_weight = total_weight / n_groups;
        let groups: number[][] = [];
        for (let k = 2; k < input.length; k++) {
            groups = [];
            for (const comb of combinations_without_repetition(input, k)) {
                const sum = comb.reduce((r, x) => x + r, 0);
                if (sum === group_weight) {
                    groups.push(comb);
                }
            }

            if (groups.length === 0) {
                continue;
            } else {
                break;
            }
        }
        let min_qe = Infinity;
        for (const g of groups) {
            min_qe = Math.min(
                min_qe,
                g.reduce((r, x) => r * x, 1)
            );
        }
        return min_qe;
    }

    tests(): boolean {
        let test_input = [1, 2, 3, 4, 5, 7, 8, 9, 10, 11];

        if (this.find_min_qe(test_input) !== 99) {
            return false;
        }

        if (this.find_min_qe(test_input, 4) !== 44) {
            return false;
        }
        return true;
    }

    part1(input: string): number {
        return this.find_min_qe(input.split("\n").map((x) => parseInt(x)));
    }

    part2(input: string): number {
        return this.find_min_qe(
            input.split("\n").map((x) => parseInt(x)),
            4
        );
    }
}
