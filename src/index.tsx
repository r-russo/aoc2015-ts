import React from "react";
import ReactDOM from "react-dom";
import { Day } from "./solutions/base";
import { Day1 } from "./solutions/day1";
import { Day2 } from "./solutions/day2";
import { Day3 } from "./solutions/day3";
import { Day4 } from "./solutions/day4";
import { Day5 } from "./solutions/day5";
import { Day6 } from "./solutions/day6";
import { Day7 } from "./solutions/day7";
import { Day8 } from "./solutions/day8";
import { Day9 } from "./solutions/day9";
import { Day10 } from "./solutions/day10";
import { Day11 } from "./solutions/day11";
import { Day12 } from "./solutions/day12";
import { Day13 } from "./solutions/day13";
import { Day14 } from "./solutions/day14";
import { Day15 } from "./solutions/day15";
import { Day16 } from "./solutions/day16";
import { Day17 } from "./solutions/day17";
import { Day18 } from "./solutions/day18";
import { Day19 } from "./solutions/day19";
import { Day20 } from "./solutions/day20";
import { Day21 } from "./solutions/day21";
import { Day22 } from "./solutions/day22";
import { Day23 } from "./solutions/day23";
import { Day24 } from "./solutions/day24";
import { Day25 } from "./solutions/day25";

interface SolutionProps {
    nday: number;
    tests_fun: () => boolean;
    part1_fun: (input: string) => number | string;
    part2_fun: (input: string) => number | string;
}

interface SolutionState {
    test_result?: string;
    test_time?: string;
    part1_result?: string;
    part1_time?: string;
    part2_result?: string;
    part2_time?: string;
    collapsed?: boolean;
}

interface PartProps {
    name: string;
    type?: string;
    result: string;
    time_elapsed: string;
    solve_fun: () => void;
}

class Solutions extends React.Component {
    days: Day[];
    constructor(props: any) {
        super(props);
        this.days = [
            new Day1(),
            new Day2(),
            new Day3(),
            new Day4(),
            new Day5(),
            new Day6(),
            new Day7(),
            new Day8(),
            new Day9(),
            new Day10(),
            new Day11(),
            new Day12(),
            new Day13(),
            new Day14(),
            new Day15(),
            new Day16(),
            new Day17(),
            new Day18(),
            new Day19(),
            new Day20(),
            new Day21(),
            new Day22(),
            new Day23(),
            new Day24(),
            new Day25(),
        ].reverse();
    }

    render() {
        const solutions = this.days.map((day) => {
            return (
                <Solution
                    nday={day.nday}
                    tests_fun={() => day.tests()}
                    part1_fun={(input) => day.part1(input)}
                    part2_fun={(input) => day.part2(input)}
                />
            );
        });
        return (
            <div className="solutions">
                <h1>Advent of Code 2015 solutions</h1>
                {solutions}
            </div>
        );
    }
}

class Solution extends React.Component<SolutionProps, SolutionState> {
    filename: string;
    constructor(props: SolutionProps) {
        super(props);
        this.filename = "inputs/day" + this.props.nday.toString() + ".txt";
        this.state = {
            test_result: "",
            test_time: "",
            part1_result: "",
            part1_time: "",
            part2_result: "",
            part2_time: "",
            collapsed: true,
        };
    }

    solve_tests() {
        const start = performance.now();
        const result = this.props.tests_fun();
        const end = performance.now();
        let state: SolutionState = {};
        if (result) {
            state.test_result = "passed";
            state.test_time = (end - start).toString() + " ms";
        } else {
            state.test_result = "failed";
            state.test_time = "N/A";
        }
        this.setState(state);
    }

    solve_part1() {
        fetch(this.filename)
            .then((resp) => resp.text())
            .then((data) => {
                const start = performance.now();
                const result = this.props.part1_fun(data.trim());
                const end = performance.now();
                const state: SolutionState = {
                    part1_result: result.toString(),
                    part1_time: (end - start).toString() + " ms",
                };
                this.setState(state);
            });
    }

    solve_part2() {
        fetch(this.filename)
            .then((resp) => resp.text())
            .then((data) => {
                const start = performance.now();
                const result = this.props.part2_fun(data.trim());
                const end = performance.now();
                const state: SolutionState = {
                    part2_result: result.toString(),
                    part2_time: (end - start).toString() + " ms",
                };
                this.setState(state);
            });
    }

    toggle() {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    render() {
        const is_collapsed = this.state.collapsed;
        return (
            <div className="solution">
                <button onClick={() => this.toggle()}>
                    {is_collapsed ? "+" : "-"} Day {this.props.nday}
                </button>
                <div className={"parts" + (is_collapsed ? "" : " active")}>
                    <Part
                        name={"Tests"}
                        type="tests"
                        result={this.state.test_result}
                        time_elapsed={this.state.test_time}
                        solve_fun={() => this.solve_tests()}
                    />
                    <Part
                        name={"Part 1"}
                        result={this.state.part1_result}
                        time_elapsed={this.state.part1_time}
                        solve_fun={() => this.solve_part1()}
                    />
                    <Part
                        name={"Part 2"}
                        result={this.state.part2_result}
                        time_elapsed={this.state.part2_time}
                        solve_fun={() => this.solve_part2()}
                    />
                </div>
            </div>
        );
    }
}

function Part(props: PartProps) {
    let result_label: JSX.Element;
    if (props.type == "tests") {
        if (props.result == "passed") {
            result_label = <label className="test_result passed">{props.result}</label>;
        } else if (props.result == "failed") {
            result_label = <label className="test_result failed">{props.result}</label>;
        }
    } else {
        result_label = <label>{props.result}</label>;
    }
    return (
        <div className="part">
            <h3>{props.name}</h3>
            <div className="field">
                Result:
                {result_label}
            </div>
            <div className="field">
                Time elapsed:
                <label>{props.time_elapsed}</label>
            </div>
            <button onClick={props.solve_fun}>Solve</button>
        </div>
    );
}

ReactDOM.render(<Solutions />, document.getElementById("root"));
